import { assets } from './assets.js';
import { getComponentPane, switchToScreen } from './screen-management.js'
import { elementDoesThing, regressToMean } from './utils.js';

//----------------------
// Dice logic
//----------------------

let dice = [];
let diceTotals = [];
zeroDice();

let lastRoll = 0;

const diceDiameter = 24;
const diceRadius = diceDiameter / 2;
const diceTranslationalAcceleration = 0.18;
const diceRotationalAcceleration = 0.025; 

let Dice = class {
    constructor(dCount) {
        this.dCount = dCount;
        this.dFace = 0;
        this.speed = Math.random() * 2.0 + 2.0; // pixels per frame
        this.direction = (Math.random() * 2 * Math.PI); // radians
        this.rVelocity =  Math.random() * 0.5 - 0.25; // radians per frame
        this.x = Math.floor(Math.random() * (graphicsCanvasWidth - diceDiameter));
        this.y = Math.floor(Math.random() * (graphicsCanvasHeight - diceDiameter));
        this.r = Math.random() * 2 * Math.PI;
        randomizeDiceFace(this);
    }
};

function drawDice(d, ctx)
{   
    let diceColor = 'rgb(0, 0, 0)';

    if(d.dCount == 2) {diceColor = 'rgb(138, 43, 226)';}
    if(d.dCount == 4) {diceColor = 'rgb(87, 215, 172)';}
    if(d.dCount == 6) {diceColor = 'rgb(227, 222, 59)';}
    if(d.dCount == 8) {diceColor = 'rgb(139, 0, 0)';}
    if(d.dCount == 10) {diceColor = 'rgb(34, 139, 34)';}
    if(d.dCount == 12) {diceColor = 'rgb(30, 30, 149)';}
    if(d.dCount == 20) {diceColor = 'rgb(240, 128, 128)';}
    if(d.dCount == 100) {diceColor = 'rgb(205, 133, 63)';}

    ctx.save();
        ctx.translate(d.x, d.y);
        ctx.rotate(d.r);
        ctx.textAlign = 'center';

        ctx.strokeStyle = diceColor;
        ctx.lineWidth = 2;
        ctx.strokeRect(0 - diceRadius, 0 - diceRadius, diceDiameter, diceDiameter);
        
        ctx.fillStyle = diceColor;
        ctx.font = '10px sans-serif';
        ctx.fillText(d.dFace ? d.dFace : '?', 12 - diceRadius, 16 - diceRadius);
    ctx.restore();
}

function randomizeDiceFace(d)
{
    d.dFace = Math.floor(Math.random() * d.dCount + 1);
}

function randomizeDiceVelocity(d, magnitude = 1.0, direction = -1) {
    d.speed = Math.max(Math.random() * 8.0 + magnitude, 5.5);
    
    d.direction = (Math.random() * 2.0 * Math.PI);
    if(direction !== -1) {
        d.direction = direction;
    }

    d.rVelocity = Math.random() * 2.0 - 1.0;
    if(Math.abs(d.rVelocity) < 0.25) {
        d.rVelocity = 0.25;
    }
}

function simulateDiceCanvas()
{
    let noDiceMoving = true;
    
    for(let i = 0; i < dice.length; i++) {
        const d = dice[i];

        let crossedMidnight = (d.r < Math.PI / 2);

        d.x += Math.cos(d.direction) * d.speed;
        d.y += Math.sin(d.direction) * d.speed;
        d.r = (d.r + d.rVelocity) % (2 * Math.PI);

        if(crossedMidnight && (d.r < Math.PI / 2)) {
            crossedMidnight = false;
        }

        if(crossedMidnight || Math.abs(d.rVelocity) > 0.25) {
            randomizeDiceFace(d);
        }
        
        d.speed = regressToMean(d.speed, 0, diceTranslationalAcceleration);
        d.rVelocity = regressToMean(d.rVelocity, 0, diceRotationalAcceleration);

        const xMin = 0 + diceRadius - 3;
        const xMax = graphicsCanvasWidth - diceRadius + 3;
        const yMin = 0 + diceRadius - 3;
        const yMax= graphicsCanvasHeight - diceRadius + 3;

        // Very dirty collision resolution
        // We only want to do collision detection if we have a relatively small number of dice;
        // Firstly because it's expensive O(n2), and secondly because there may be insufficient space (on-screen) to settle all dice without overlap.
        if(dice.length <= 30) {  
            for(let i = 0; i < dice.length; i++) {
                for(let j = 0; j < dice.length; j++) {
                    const d1 = dice[i];
                    const d2 = dice[j];
    
                    if(i == j) {
                        continue;
                    }
    
                    if(!(
                        d1.x > d2.x + diceDiameter ||
                        d1.x + diceDiameter < d2.x ||
                        d1.y > d2.y + diceDiameter ||
                        d1.y + diceDiameter < d2.y 
                    )) {
                        if(d1.speed == 0 && d2.speed == 0) {
                            d1.speed = 3.0;
                            d1.rVelocity = Math.random() * 0.5;
                            randomizeDiceFace(d1);
                            randomizeDiceFace(d2);
                        }
                    }
                }
            }
        }
        // End very dirty collision resolution

        if(d.x < xMin) {
            d.x = xMin;
            d.direction = Math.PI - d.direction;
            randomizeDiceFace(d);
        }
        if(d.x > xMax) {
            d.x = xMax;
            d.direction = Math.PI - d.direction;
            randomizeDiceFace(d);
        }
        if(d.y < yMin) {
            d.y = yMin;
            d.direction = -d.direction;
            randomizeDiceFace(d);
        }
        if(d.y > yMax) {
            d.y = yMax;
            d.direction = -d.direction;
            randomizeDiceFace(d);
        }

        if(d.speed || d.rVelocity) {
            noDiceMoving = false;
        }
    }

    if(noDiceMoving) {
        framesSettled++;

        if(framesSettled == 5) {
            if(dice.length > 0) {
                lastRoll = addUpPhysicalDice();
                addDiceMessage("Total: " + lastRoll);
            }

            framesSettled = 99;
        }
    }
}

export function getLastDiceRoll()
{
    return lastRoll;
}

function getDiceSummary(tense = "future")
{
    let diceSummary = ``;
    
    if(tense == "future") {
        diceSummary += `You will roll`;
    }
    else {
        diceSummary += `Rolling`;
    }
    
    diceTotals.forEach(function (element, index) {
        if(index == 1) {
            return;
        }

        if(element > 0) {
            diceSummary += ` ${element}d${index} +`;
        }
    });

    if(diceSummary.indexOf('+') !== -1){
        diceSummary = diceSummary.substring(0, diceSummary.length - 2);
    }

    const mod = diceTotals[1];
    if(mod) {
        diceSummary += `${mod < 0 ? '' : ' +'} ${mod}`;
    }

    diceSummary += tense == "future" ? '.' : '...';
    diceSummary = diceSummary.replace('-', '- ');

    if(dice.length == 0) {   // If no dice were added
        diceSummary = `Add dice below:`;
    }

    if(diceSummary.length > 60) {
        if(tense == 'future') {
            diceSummary = `You will roll ${dice.length} dice.`;
        }
        else {
            diceSummary = `Rolling ${dice.length} dice...`;
        }
    }

    return diceSummary;
}

function addUpPhysicalDice()
{
    let total = 0;

    for(let i = 0; i < dice.length; i++) {
        total += dice[i].dFace;
    }

    total += diceTotals[1];

    return total;
}

export function addDiceToTable(dn, n)
{   
    if(dn == 1) {
        diceTotals[1] += n;
        return;
    }

    if(messages.length == 0) {
        addDiceMessage("Tap panel above to roll.");
    }
    
    for(let i = 0; i < n; i++) {
        diceTotals[dn]++;
        const newDice = new Dice(dn);
        dice.push(newDice);
    }
}

function removeDiceFromTable(dn, n)
{
    if(dn == 1) {
        diceTotals[1] -= n;
        return;
    }
    
    let remainingDiceToDrop = n;

    for(let i = 0; i < dice.length; i++) {
        const d = dice[i];

        if(d.dCount == dn) {
            dice.splice(i, 1);
            remainingDiceToDrop -= 1;
            i--;    // we don't want to advance cursor since we just dropped the current indexed item
            diceTotals[dn]--;

            if(remainingDiceToDrop <= 0) {
                break;
            }
        }
    }
}

function zeroDice()
{
    dice = [];

    diceTotals[1] = 0;
    diceTotals[2] = 0;
    diceTotals[4] = 0;
    diceTotals[6] = 0;
    diceTotals[8] = 0;
    diceTotals[10] = 0;
    diceTotals[12] = 0;
    diceTotals[20] = 0;
    diceTotals[100] = 0;
}

//----------------------
// Graphics pane
//----------------------

let graphicsCanvasWidth = 0;
let graphicsCanvasHeight = 0;
let graphicsCanvasLoop = true;
let timeOutActive = false;

let framesSettled = 99;

export function setGraphicsLoop(trueFalse)
{
    graphicsCanvasLoop = trueFalse;
}

function renderDiceCanvas()
{
    simulateDiceCanvas();
    
    var c = document.getElementById("dice-graphics-canvas");
    
    if(!c) {
        return;
    }

    var ctx = c.getContext("2d");

    ctx.fillStyle = 'rgb(255, 255, 255)';
    ctx.fillRect(0, 0, graphicsCanvasWidth, graphicsCanvasHeight);

    for(let i = 0; i < dice.length; i++) {
        drawDice(dice[i], ctx);
    }

    if(graphicsCanvasLoop && !timeOutActive) {
        setTimeout(function () {
            timeOutActive = false;
            renderDiceCanvas();
        }, 16);
        timeOutActive = true;
    }

    // Draw box outline
    const outlineWidth = 2;
    ctx.fillStyle = 'rgb(110, 110, 110)';
    ctx.fillRect(0, 0, outlineWidth, graphicsCanvasHeight);
    ctx.fillRect(graphicsCanvasWidth - outlineWidth, 0, outlineWidth, graphicsCanvasHeight);
    ctx.fillRect(0, 0, graphicsCanvasWidth, outlineWidth);
    ctx.fillRect(0, graphicsCanvasHeight - outlineWidth, graphicsCanvasWidth, outlineWidth);

    const chinkWidth = 1;
    ctx.fillStyle = 'rgb(255, 255, 255)';
    ctx.fillRect(0, 0, chinkWidth, chinkWidth);
    ctx.fillRect(graphicsCanvasWidth - chinkWidth, 0, chinkWidth, chinkWidth);
    ctx.fillRect(0, graphicsCanvasHeight - chinkWidth, chinkWidth, chinkWidth);
    ctx.fillRect(graphicsCanvasWidth - chinkWidth, graphicsCanvasHeight - chinkWidth, chinkWidth, chinkWidth);
}

function resizeDiceCanvas()  // Resets canvas width and height
{
    const graphicsCanvas = document.getElementById("dice-graphics-canvas");

    if(!graphicsCanvas) {
        return;
    }

    const viewportWidth = window.innerWidth;
    const viewportHeight = window.innerHeight;

    graphicsCanvasWidth = Math.round((viewportWidth - 48) * 0.5);
    graphicsCanvasHeight = Math.round((viewportHeight - 48 - 49) * 0.6);

    graphicsCanvas.width = graphicsCanvasWidth;
    graphicsCanvas.height = graphicsCanvasHeight;
}

function handleWindowResizeDice()
{
    resizeDiceCanvas();
}
window.addEventListener('resize', handleWindowResizeDice);

function handleDicePaneClick(event)
{
    const canvas = event.target;

    const rect = canvas.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;

    for(let i = 0; i < dice.length; i++) {
        const d = dice[i];
        const distanceSquared = (d.x - x) * (d.x - x) + (d.y - y) * (d.y - y);

        const dx = d.x - x;
        const dy = d.y - y;
        let angle = Math.atan2(dy, dx);

        const power = Math.min(11.0 / (distanceSquared / 800), 11.0);
        if(power < 0.5) {
            angle = -1;
        }

        randomizeDiceFace(d);
        randomizeDiceVelocity(d, power, angle);
    }

    framesSettled = 0;
    
    if(dice.length > 0) {
        addDiceMessage(getDiceSummary("present"));
    }
}

//----------------------
// Messages pane
//----------------------

let messages = [];

function buildDiceMessagesPane()
{
    const messagesPane = document.getElementById("dice-messages-pane");
    const oldDicemessages = document.getElementById("dice-messages");

    if(!messagesPane) {
        return;
    }

    var newDiceMessages = document.createElement("div");
    newDiceMessages.id = "dice-messages";
    
    for(let i = messages.length - 1; i >=0; i--) {
        const reverseIndex = (messages.length - 1) - i;
        
        var opacity = 1.0 * Math.pow(0.8, reverseIndex);

        var p = document.createElement("p");
        p.innerText = messages[i];
        p.style.opacity = opacity;
        if(reverseIndex == 0) {
            p.style.fontWeight = 'bolder';
        }

        newDiceMessages.appendChild(p);
    }

    messagesPane.removeChild(oldDicemessages);
    messagesPane.appendChild(newDiceMessages);
}

export function addDiceMessage(str)
{
    messages.push(str);

    if(messages.length > 50) {
        messages.splice(0, 25);
    }

    buildDiceMessagesPane();    
}

function clearDiceMessages()
{
    messages = [];
    buildDiceMessagesPane();
}

//-------------------------
// Build Html Components
//-------------------------

function generateDiceQuantifierWidget(dn)
{
    const diceTitle = dn === 1 ? 'Mod.' : `D${dn}`;
    const currentN = ((dn === 1) && (diceTotals[1] > 0) ? '+' : '') + `${diceTotals[dn]}`;
    
    return `
    <div class="dice-quantifier-widget" dn="${dn}">
        <div class="dice-title">${diceTitle}</div>
        <div class="dice-quantifier-buttons">
            <div class="minus-five button" dn="${dn}" mod="-5"> << </div>
            <div class="minus-one button" dn="${dn}" mod="-1"> < </div>
            <div class="current-n"> ${currentN} </div>
            <div class="plus-one button" dn="${dn}" mod="+1"> > </div>
            <div class="plus-five button" dn="${dn}" mod="+5"> >> </div>
        </div>
    </div>
    `;
}

function buildDiceQuantifiersPane()
{
    let diceSummary = getDiceSummary();
    
    const html = `
    <div id="dice-summary">${diceSummary}</div>
    <div id="quantifiers-section">
        <div class="dice-quantifiers-pane-column">
            ${generateDiceQuantifierWidget(2)}
            ${generateDiceQuantifierWidget(4)}
            ${generateDiceQuantifierWidget(6)}
            ${generateDiceQuantifierWidget(8)}
            ${generateDiceQuantifierWidget(1)}
        </div>
        <div class="dice-quantifiers-pane-column">
            ${generateDiceQuantifierWidget(10)}
            ${generateDiceQuantifierWidget(12)}
            ${generateDiceQuantifierWidget(20)}
            ${generateDiceQuantifierWidget(100)}
            <div id="remove-all-dice-button" class="button">Remove all</div>
        </div>
    </div>
    `;

    document.getElementById("dice-quantifiers-pane").innerHTML = html;
    elementDoesThing(".dice-quantifier-buttons .button", "click", handleDiceQuantifierButtonClick);
    elementDoesThing("#remove-all-dice-button", "click", handleRemoveAllButtonClick);
}

function handleDiceQuantifierButtonClick(event)
{
    const dn = event.target.getAttribute("dn");
    const mod = parseInt(event.target.getAttribute("mod"));

    if(mod > 0) {
        addDiceToTable(dn, mod);
    }

    if(mod < 0) {
        removeDiceFromTable(dn, -mod);
    }

    buildDiceQuantifiersPane();
}

function handleRemoveAllButtonClick()
{
    zeroDice();
    buildDiceQuantifiersPane();
}

function applyDiceRollToCharacter()
{
    switchToScreen('characters');
}

function applyDiceRollToOpponent()
{
    switchToScreen('opponents');
}

export function buildDiceScreen()
{    
    const html = `
    <div id="dice-graphics-messages-panes">
        <div id="dice-graphics-pane">
            <canvas id="dice-graphics-canvas" width="0" height="0"></canvas>
        </div>
        <div id="dice-spacer-pane"></div>
        <div id="dice-messages-pane">
            <div id="dice-messages-clear" class="button gray-variant">Clear log</div>
            <div id="dice-messages"></div>
        </div>
    </div>
    <div id="dice-custom-pane">
        <div id="dice-quantifiers-pane"></div>
        <div id="dice-actions-pane">
            <div id="actions-message">Use last roll to:</div>
            <div id="dice-actions-columns">
                <div class="action-column">
                    <div class="button character-button" action="hit-miss-character">Hit/miss character</div>
                    <div class="button opponent-button" action="hit-miss-opponent">Hit/miss opponent</div>
                </div>
                <div class="action-column">
                    <div class="button red-variant character-button" action="damage-character">Damage character</div>
                    <div class="button red-variant opponent-button" action="damage-opponent">Damage opponent</div>
                </div>
                <div class="action-column">
                    <div class="button green-variant character-button" action="heal-character">Heal character</div>
                    <div class="button green-variant opponent-button" action="heal-opponent">Heal opponent</div>
                </div>
            </div>
        </div>
    </div>
    `;

    getComponentPane().innerHTML = html;

    buildDiceQuantifiersPane();

    resizeDiceCanvas();
    simulateDiceCanvas();
    renderDiceCanvas();
    elementDoesThing("#dice-graphics-canvas", "click", handleDicePaneClick);

    elementDoesThing(".action-column .character-button", "click", applyDiceRollToCharacter);
    elementDoesThing(".action-column .opponent-button", "click", applyDiceRollToOpponent);

    buildDiceMessagesPane();
    elementDoesThing("#dice-messages-clear", "click", clearDiceMessages);
}