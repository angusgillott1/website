//-------------------
// JQuery substitute
//-------------------

export function elementDoesThing(selector, event, fn)
{
    if(!selector) {
        return;
    }

    if(typeof(selector) !== 'string') {
        selector.addEventListener(event, fn);   // covers case where a DOM object is passed in
        return;
    }

    const matchingElements = document.querySelectorAll(selector);
    matchingElements.forEach(function(element) {
        element.addEventListener(event, fn);
    });
}

//----------------------
// Maths
//----------------------

export function regressToMean(val, mean, step)
{
    if(val > mean) {
        val -= step;
        if(val < mean) {
            val = mean;
        }
    }

    if(val < mean) {
        val += step;
        if(val > mean) {
            val = mean;
        }
    }

    return val;
}

export function generateAlphabetString(n = 10)
{
    let str = ``;
    for(let i = 0; i < n; i++) {
        str += String.fromCharCode(Math.floor(Math.random() * 26) + 97);
    }

    return str;
}

export function dropHtmlTags(str)
{
    if(typeof(str) !== 'string') {
        return str;
    }

    return str.replace(/</g, '').replace(/>/g, '');
}

//-----------------------
// Local storage
//-----------------------

export function saveVariable(name, variable)
{
    localStorage.setItem(name, JSON.stringify(variable));
}

export function retrieveVariable(name)
{
    let value = localStorage.getItem(name);

    if(!typeof(value) == 'string') 
    {
        return value;
    }

    return JSON.parse(value);
}