import { assets } from './assets.js';
import { buildDiceScreen, setGraphicsLoop } from './dice.js'
import { buildOpponentsScreen, setOpponentsMode } from './opponents.js';
import { buildNotesScreen } from './notes.js';

//----------------------
// Globals
//----------------------

let currentScreen = 'dice';

//-------------------------
// Component Pane management
//-------------------------

export function getComponentPane()
{
    const componentPane = document.getElementById("component-pane");
    return componentPane;
}

export function purgeComponentPane()
{
    getComponentPane().innerHTML = ``;
}

//------------------------
// Screen management
//------------------------

export function switchToScreen(screenType)
{
    currentScreen = screenType;

    document.querySelectorAll("button.switch-tab-button").forEach(function(element) {
        const tabAttribute = element.getAttribute("tab");

        if(tabAttribute == screenType) {
            element.classList.add("active-tab");
        }
        else {
            element.classList.remove("active-tab");
        }
    });

    purgeComponentPane();

    if(screenType == 'dice') {
        setGraphicsLoop(true);
        buildDiceScreen();
    }
    if(screenType == 'characters') {
        setGraphicsLoop(false);
        setOpponentsMode('character');
        buildOpponentsScreen();
    }
    if(screenType == 'opponents') {
        setGraphicsLoop(false);
        setOpponentsMode('opponent');
        buildOpponentsScreen();
    }
    if(screenType == 'notes') {
        setGraphicsLoop(false);
        buildNotesScreen();
    }
}