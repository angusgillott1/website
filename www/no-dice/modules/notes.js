import { getComponentPane } from './screen-management.js'
import { elementDoesThing, retrieveVariable, saveVariable } from './utils.js';

let notes = ``;

function retrieveNotesFromSessionStorage()
{
    let storedNotes = retrieveVariable('notes');
    if(storedNotes) {
        notes = storedNotes;
    }
}
retrieveNotesFromSessionStorage();

function saveNotesToSessionStorage()
{
    saveVariable('notes', notes);
}

function storeNotesFieldToVariable(event) {
    notes = event.target.value;
    saveNotesToSessionStorage();
}

export function buildNotesScreen()
{   
    const html = `
    <div id="notes-pane">
        <textarea id="notes-text-area" name="notes-text-area" rows="1" cols="1" maxlength="50000" placeholder="Enter any notes here."></textarea>
    </div>
    `;

    getComponentPane().innerHTML = html;

    const textArea = document.getElementById("notes-text-area");
    document.getElementById("notes-text-area").textContent = notes;

    elementDoesThing(textArea, "input", storeNotesFieldToVariable);
}