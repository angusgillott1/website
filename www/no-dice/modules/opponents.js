//-------------------------
// Opponents Logic
//-------------------------

import { assets } from './assets.js';
import { getLastDiceRoll } from "./dice.js";
import { getComponentPane } from './screen-management.js'
import { dropHtmlTags, generateAlphabetString, retrieveVariable, saveVariable } from './utils.js';
import { elementDoesThing } from './utils.js';

let charactersData = [
    {name: 'Robert the Ranger', currentHp: 40, maxHp: 44, armor: 16, id: generateAlphabetString()},
    {name: 'Maisie the Mage', currentHp: 32, maxHp: 32, armor: 12, id: generateAlphabetString()},
];

let opponentsData = [
    {name: 'Ancient Dragon', currentHp: 87, maxHp: 100, armor: 25, challenge: 'Impossible', id: generateAlphabetString()},
];

function retrieveOpponentsDataFromSessionStorage()
{
    let storedOpponents = retrieveVariable('opponents');
    if(storedOpponents) {
        opponentsData = storedOpponents;
    }

    let storedCharacters = retrieveVariable('characters');
    if(storedCharacters) {
        charactersData = storedCharacters;
    }
}
retrieveOpponentsDataFromSessionStorage();

function saveOpponentsDataToSessionStorage()
{
    saveVariable('opponents', opponentsData);
    saveVariable('characters', charactersData);
}

let opponents = opponentsData;  // we'll point the opponents variable reference to either the charactersData array OR the opponentsData array

let opponentsMode = 'opponent';
export function setOpponentsMode(m)
{
    opponentsMode = m;
    
    if(opponentsMode == 'opponent') {
        opponents = opponentsData;
    }
    else {
        opponents = charactersData;
    }
}

let Opponent = class {
    constructor(name = ``, maxHp = 0, armor = 0, challenge = 'unspecified') {
        this.name = name;
        this.currentHp = maxHp;
        this.maxHp = maxHp;
        this.armor = armor;
        this.challenge = challenge;
        this.id = generateAlphabetString();
    }
};

export function getOpponents()
{
    return opponents;
}

//-------------------------
// Build Html Components
//-------------------------

function determineInjuryText(currentHp, maxHp) {
    if(currentHp === undefined || !maxHp) {
        return ``;
    }

    const p = currentHp / maxHp;
    
    if(p > 1.0) return `Bonus hitpoints`;    
    if(p == 1.0) return `Not injured`;
    if(p > 0.75) return `Slightly wounded`;
    if(p > 0.5) return `Moderately wounded`;
    if(p > 0.25) return `Badly wounded`;
    if(p > 0.0) return `Critically wounded`;

    return `Down`;
}

function determineInjuryColor(currentHp, maxHp) {
    if(!currentHp || !maxHp) return `rgb(0, 0, 0)`;

    const p = currentHp / maxHp;
    
    if(p > 1.0) return `rgb(0, 255, 0)`;
    if(p == 1.0) return `rgb(0, 190, 0)`;
    if(p > 0.75) return `rgb(200, 130, 0)`;
    if(p > 0.5) return `rgb(150, 50, 0)`;
    if(p > 0.25) return `rgb(180, 0, 0)`;
    if(p > 0.0) return `rgb(250, 0, 0)`;

    return `rgb(0, 0, 0)`;
}

function generateOpponentCard(opponent, isSpacerCard = false)
{     
    return `
    <div class="opponent-card" ${isSpacerCard ? 'style="visibility: hidden"' : ''}>
        <div class="opponent-card-header-section">
            <div class="opponent-name">${opponent.name}</div>
            <div class="roll-action-buttons">
                <div class="apply-roll-damage button red-variant" opponent="${opponent.id}">Apply damage</div>
                <div class="apply-roll-damage button green-variant" opponent="${opponent.id}" healInstead=true >Apply heal</div>
            </div>
            <div class="remove-opponent button red-variant" opponent="${opponent.id}">&#10060</div>
        </div>
        <div class="opponent-card-hp-armor-section">
            <div class="opponent-card-hp-section">
                <div class="opponent-hp-buttons">
                    <div class="minus-five button" mod="-5" type="hp" opponent="${opponent.id}"> << </div>
                    <div class="minus-one button" mod="-1" type="hp" opponent="${opponent.id}"> < </div>
                    <div class="opponent-hp">HP: ${opponent.currentHp} / ${opponent.maxHp}</div>
                    <div class="plus-one button" mod="+1" type="hp" opponent="${opponent.id}"> > </div>
                    <div class="plus-five button" mod="+5" type="hp" opponent="${opponent.id}"> >> </div>
                </div>
            </div>
            <div class="opponent-card-armor-section">
                <div class="opponent-armor-buttons">
                    <div class="minus-five button" mod="-5" type="armor" opponent="${opponent.id}"> << </div>
                    <div class="minus-one button" mod="-1" type="armor" opponent="${opponent.id}"> < </div>
                    <div class="opponent-armor">Armor: ${opponent.armor}</div>
                    <div class="plus-one button" mod="+1" type="armor" opponent="${opponent.id}"> > </div>
                    <div class="plus-five button" mod="+5" type="armor" opponent="${opponent.id}"> >> </div>
                </div>
            </div>
        </div>
        <div class="opponent-card-damage-text" style="color: ${determineInjuryColor(opponent.currentHp, opponent.maxHp)}">${determineInjuryText(opponent.currentHp, opponent.maxHp)}</div>
    </div>
    `;
}

function generateOpponentsColumn(opponents, uneven = false)
{
    let html = `<div class="opponents-column">`;

    for(let i = 0; i < opponents.length; i++) {
        html += generateOpponentCard(opponents[i]);
    }

    if(uneven) {
        html += generateOpponentCard({}, true);   // generate a placeholder card
    }

    html += `</div>`;
    
    return html;
}

function getIndexOfOpponentWithId(id) {
    for(let i = 0; i < opponents.length; i++) {
        if(opponents[i].id == id) {
            return i;
        }
    }

    return -1;
}

function handleHpOrArmorModifierClick(event)
{   
    const t = event.target.getAttribute("type");
    const mod = parseInt(event.target.getAttribute("mod"));
    const oid = event.target.getAttribute("opponent");

    const opponent = opponents[getIndexOfOpponentWithId(oid)];

    if(t == 'armor') {
        opponent.armor = parseInt(opponent.armor) + parseInt(mod);
    }

    if(t == 'hp') {
        opponent.currentHp = parseInt(opponent.currentHp) + parseInt(mod);
    }

    saveOpponentsDataToSessionStorage();
    buildOpponentsScreen();
}

function handleOpponentsRemoveAllButtonClick()
{
    opponents.length = 0;

    saveOpponentsDataToSessionStorage();
    buildOpponentsScreen();
}

function handleOpponentAddClick()
{
    document.querySelector("#add-opponent-modal").classList.add("showing");
}

function handleOpponentAddSubmitClick()
{   
    const nameField = document.querySelector("#opponent-name");
    const hpField = document.querySelector("#max-hp");
    const armorField = document.querySelector("#armor");
    
    const name = dropHtmlTags(nameField.value);
    const maxHp = hpField.value;
    const armor = armorField.value;

    if(!name || !maxHp || !armor) {
        return;
    }

    opponents.push(new Opponent(name, parseInt(maxHp), parseInt(armor)));

    document.querySelector("#add-opponent-modal").classList.remove("showing");
    nameField.value = '';
    hpField.value = '';
    armorField.value = '';

    saveOpponentsDataToSessionStorage();
    buildOpponentsScreen();
}

function handleOpponentAddModalClickAway(event)
{   
    if (event.target.closest("#add-opponent-modal-inner")) {
        return;
    }
    
    event.stopPropagation();
    
    document.querySelector("#add-opponent-modal").classList.remove("showing");
    document.querySelector("#opponent-name").value = '';
    document.querySelector("#max-hp").value = '';
    document.querySelector("#armor").value = '';
}

function handleOpponentRemoveSingleClick(event)
{
    const oid = event.target.getAttribute("opponent");
    const oi = getIndexOfOpponentWithId(oid);

    opponents.splice(oi, 1);

    saveOpponentsDataToSessionStorage();
    buildOpponentsScreen();
}

function handleOpponentDamageClick(event)
{   
    const oid = event.target.getAttribute("opponent");
    const oi = getIndexOfOpponentWithId(oid);

    const healInstead = event.target.getAttribute("healInstead");

    if(!healInstead) {
        opponents[oi].currentHp -= getLastDiceRoll();
    }
    else {
        opponents[oi].currentHp += getLastDiceRoll();
    
        if(opponents[oi].currentHp > opponents[oi].maxHp) {
            opponents[oi].currentHp = opponents[oi].maxHp;
        }
    }

    saveOpponentsDataToSessionStorage();
    buildOpponentsScreen();
}

export function buildOpponentsScreen()
{   
    let rollMessage = ``;
    rollMessage = `Your last roll was ${getLastDiceRoll()}.`;

    let leftOpponents = [];
    let rightOpponents = [];

    for(let i = 0; i < opponents.length; i++) {
        if(i % 2 == 0) {
            leftOpponents.push(opponents[i]);
        }
        else {
            rightOpponents.push(opponents[i]);
        }
    }

    const html = `
    <div id="opponents-header">
        <div id="opponents-header-dice-roll">${rollMessage}</div>
    </div>
    <div class="opponents-spacer-pane"></div>
    <div id="opponent-cards-pane">
        ${generateOpponentsColumn(leftOpponents)}
        ${generateOpponentsColumn(rightOpponents, (opponents.length % 2 != 0))}
    </div>
    <div id="add-remove-opponents-section">
        <div id="add-opponent" class="button green-variant">Add new</div>
        <div id="remove-opponents" class="button red-variant" ${opponents.length == 0 ? `style="display: none;"` : ``}>Remove all</div>
    </div>
    <div id="add-opponent-modal">
        <div id="add-opponent-modal-inner">
            <div id="add-opponent">Add ${opponentsMode}:</div>
            <label for="name">Name:</label>
            <input class="form-input" type="text" id="opponent-name" name="opponent-name"><br><br>
            <label for="max-hp">Hit points:</label>
            <input class="form-input" type="number" id="max-hp" name="max-hp"><br><br>
            <label for="armor">Armor:</label>
            <input class="form-input" type="number" id="armor" name="armor"><br><br>
            <div id="add-opponent-confirm" class="button">Add</div>
        </div>
    </div>
    `;

    getComponentPane().innerHTML = html;

    elementDoesThing("#remove-opponents", "click", handleOpponentsRemoveAllButtonClick);
    elementDoesThing("#add-opponent", "click", handleOpponentAddClick);
    elementDoesThing(".opponent-card-hp-armor-section .button", "click", handleHpOrArmorModifierClick);
    elementDoesThing(".remove-opponent", "click", handleOpponentRemoveSingleClick);
    elementDoesThing("#add-opponent-confirm", "click", handleOpponentAddSubmitClick);
    elementDoesThing("#add-opponent-modal", "click", handleOpponentAddModalClickAway);
    elementDoesThing(".apply-roll-damage", "click", handleOpponentDamageClick);
}