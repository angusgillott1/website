import { elementDoesThing } from './modules/utils.js';
import { switchToScreen } from './modules/screen-management.js';
import { loadAllAssets } from './modules/assets.js';
import { addDiceToTable, addDiceMessage } from './modules/dice.js';

//------------------
// Fullscreen and App Launch Management
//------------------

function isCurrentlyEnteringFullscreen()
{
    // Whether we are entering or exiting fullscreen is not really in our control, as the browser or user may force it
    return !!(document.fullscreenElement);
}

function enterFullscreenCleanup()
{    
    document.body.classList.add("full-screen-mode");
}

function leaveFullscreenCleanup()
{
    document.body.classList.remove("full-screen-mode");
}

function requestFullscreen()
{
    document.documentElement.requestFullscreen();
}

function exitFullscreen()
{
    window.document.exitFullscreen();
}

function handleFullscreenChange()
{
    if(isCurrentlyEnteringFullscreen()) {
        enterFullscreenCleanup();
    }
    else {
        leaveFullscreenCleanup();
    }
}

function setupFullscreen()
{
    elementDoesThing("#launch-app-button", "click", launchApp);
    elementDoesThing("#fullscreen-enter-button", "click", requestFullscreen);
    elementDoesThing("#fullscreen-exit-button", "click", exitFullscreen);
    elementDoesThing(document, "fullscreenchange", handleFullscreenChange);``
}

function launchApp()
{
    document.body.classList.add("app-launched");
    switchToScreen('dice');

    addDiceMessage('Add dice with the buttons to the right.');
    addDiceMessage('Tap panel above to roll.');

    for(let i = 0; i < 3; i++) {
        addDiceToTable(Math.floor(Math.random() * 5 + 2) * 2, 1);
    }

    switchToScreen('dice');
}

//-----------------
// Menu buttons
//-----------------

function handleSwitchTabButtonClick(event)
{
    const tabAttribute = event.target.getAttribute("tab");

    switchToScreen(tabAttribute);
}

function setupMenuButtons() {
    elementDoesThing(".switch-tab-button", "click", handleSwitchTabButtonClick);
}

//------------------
// Booting application
//------------------

window.onload = function()
{
    loadAllAssets();
    setupFullscreen();
    setupMenuButtons();

    //  jump straight into it
    {
        launchApp();
    }
}