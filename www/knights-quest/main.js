//------------------
// Tiny font
//------------------

var letters=[];
{
letters[" "]=[[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]];letters["1"]=[[0,1,0],[0,1,0],[0,1,0],[0,1,0],[0,1,0]];letters["2"]=[[1,1,1],[0,0,1],[1,1,1],[1,0,0],[1,1,1]];letters["3"]=[[1,1,1],[0,0,1],[1,1,1],[0,0,1],[1,1,1]];letters["4"]=[[1,0,1],[1,0,1],[1,0,1],[1,1,1],[0,0,1]];letters["5"]=[[1,1,1],[1,0,0],[1,1,1],[0,0,1],[1,1,1]];letters["6"]=[[1,1,1],[1,0,0],[1,1,1],[1,0,1],[1,1,1]];letters["7"]=[[1,1,1],[0,0,1],[0,0,1],[0,0,1],[0,0,1]];letters["8"]=[[1,1,1],[1,0,1],[1,1,1],[1,0,1],[1,1,1]];letters["9"]=[[1,1,1],[1,0,1],[1,1,1],[0,0,1],[1,1,1]];letters["0"]=[[1,1,1],[1,0,1],[1,0,1],[1,0,1],[1,1,1]];letters["a"]=[[0,1,0],[1,0,1],[1,1,1],[1,0,1],[1,0,1]];letters["b"]=[[1,1,0],[1,0,1],[1,1,0],[1,0,1],[1,1,0]];letters["c"]=[[0,1,1],[1,0,0],[1,0,0],[1,0,0],[0,1,1]];letters["d"]=[[1,1,0],[1,0,1],[1,0,1],[1,0,1],[1,1,0]];letters["e"]=[[1,1,1],[1,0,0],[1,1,1],[1,0,0],[1,1,1]];letters["f"]=[[1,1,1],[1,0,0],[1,1,1],[1,0,0],[1,0,0]];letters["g"]=[[1,1,1],[1,0,0],[1,0,1],[1,0,1],[1,1,1]];letters["h"]=[[1,0,1],[1,0,1],[1,1,1],[1,0,1],[1,0,1]];letters["i"]=[[1,1,1],[0,1,0],[0,1,0],[0,1,0],[1,1,1]];letters["j"]=[[0,0,1],[0,0,1],[0,0,1],[1,0,1],[0,1,0]];letters["k"]=[[1,0,1],[1,0,1],[1,1,0],[1,0,1],[1,0,1]];letters["l"]=[[1,0,0],[1,0,0],[1,0,0],[1,0,0],[1,1,1]];letters["m"]=[[1,0,1],[1,1,1],[1,1,1],[1,0,1],[1,0,1]];letters["n"]=[[1,0,1],[1,1,1],[1,1,1],[1,1,1],[1,0,1]];letters["o"]=[[0,1,0],[1,0,1],[1,0,1],[1,0,1],[0,1,0]];letters["p"]=[[1,1,0],[1,0,1],[1,1,0],[1,0,0],[1,0,0]];letters["q"]=[[0,1,0],[1,0,1],[1,0,1],[1,1,1],[0,1,1]];letters["r"]=[[1,1,0],[1,0,1],[1,0,1],[1,1,0],[1,0,1]];letters["s"]=[[0,1,1],[1,0,0],[0,1,0],[0,0,1],[1,1,0]];letters["t"]=[[1,1,1],[0,1,0],[0,1,0],[0,1,0],[0,1,0]];letters["u"]=[[1,0,1],[1,0,1],[1,0,1],[1,0,1],[1,1,1]];letters["v"]=[[1,0,1],[1,0,1],[1,0,1],[1,0,1],[0,1,0]];letters["w"]=[[1,0,1],[1,0,1],[1,1,1],[1,1,1],[1,0,1]];letters["x"]=[[1,0,1],[1,0,1],[0,1,0],[1,0,1],[1,0,1]];letters["y"]=[[1,0,1],[1,0,1],[0,1,0],[0,1,0],[0,1,0]];letters["z"]=[[1,1,1],[0,0,1],[0,1,0],[1,0,0],[1,1,1]];letters["/"]=[[0,0,1],[0,0,1],[0,1,0],[1,1,0],[1,0,0]];letters["!"]=[[0,1,0],[0,1,0],[0,1,0],[0,0,0],[0,1,0]];letters["?"]=[[1,1,1],[0,0,1],[0,1,0],[0,0,0],[0,1,0]];letters[","]=[[0,0,0],[0,0,0],[0,0,0],[0,1,0],[1,0,0]];letters["."]=[[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,1,0]];letters[":"]=[[0,0,0],[0,0,0],[0,1,0],[0,0,0],[0,1,0]];letters["'"]=[[0,1,0],[0,1,0],[0,0,0],[0,0,0],[0,0,0]];
}

//--------
// Global variables
//--------

const SCREEN_WIDTH = 640;
const SCREEN_HEIGHT = 320;

const LOGICAL_WIDTH = 128;
const LOGICAL_HEIGHT = 64;

const BLANK = 0;
const GREEN = 1;
const ORANGE = 2;
const BLUE = 3;
const DEBUG = 4;

var dx1 = 20;
var dx2 = 80;
var dy1 = 30;
var dy2 = 50;

var canvas;
var context;
var logicalScreen;

var keysDown = new Map();
var globalTick = 0;

var currentScene = "intro";
var currentSubstate = "initializing";

var delay = 0;

//------------
// Game variables
//------------

var level;
var health;
var mana;
var knife;
var shield;
var attack;
var defense;
var potions;
var gold;
var experience;
var greenKey;
var orangeKey;

var opponentLevel = 1;
var opponentHealth = 20;
var opponentHasKey = 0;
var isFinalBattle = 0;

var saveDataExists;

function initializeVariables()
{
  level = 1;
  health = 20;
  mana = 10;
  knife = 0;
  shield = 0;
  attack = level + 9 + knife;
  defense = level + 4 + shield;
  potions = 1;
  gold = 50;
  experience = 0;
  greenKey = 0;
  orangeKey = 0;

  saveDataExists = 0;
  setCookie('saveDataExists', 0);
}

function loadVariables()
{
  if(! getCookie('saveDataExists'))
  {
    initializeVariables();
    return;
  }
  
  saveDataExists = 1;
  
  level = getCookie('level');
  health = getCookie('health');
  mana = getCookie('mana');
  knife = getCookie('knife');
  shield = getCookie('shield');
  attack = level + 9 + knife;
  defense = level + 4 + shield;
  potions = getCookie('potions');
  gold = getCookie('gold');
  experience = getCookie('experience');
  greenKey = getCookie('greenKey');
  orangeKey = getCookie('orangeKey');
}

function saveVariables()
{
  setCookie('level', level, 300);
  setCookie('health', health, 300);
  setCookie('mana', mana, 300);
  setCookie('knife', knife, 300);
  setCookie('shield', shield, 300);
  setCookie('potions', potions, 300);
  setCookie('gold', gold, 300);
  setCookie('experience', experience, 300);
  setCookie('greenKey', greenKey, 300);
  setCookie('orangeKey', orangeKey, 300);

  saveDataExists = 1;
  setCookie('saveDataExists', 1);
}

function fullRestore()
{
  health = 2 * level + 18;
  mana = 2 * level + 8;
}
//--------
// Utility functions
//--------

function setCookie(name,value,days) {
  var expires = "";
  if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "0")  + expires + "; path=/; Secure; SameSite=Strict";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) 
      {
        return parseInt(c.substring(nameEQ.length,c.length));
      }
  }
  return null;
}

function eraseCookie(name) {   
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

//--------
// Setup functions
//--------

function setupDisplayScreen()
{
	canvas = document.getElementById('game-screen');
	context = canvas.getContext('2d');
}

function setupLogicalScreen()
{
  logicalScreen = [];

  for(var x = 0; x < LOGICAL_WIDTH; x++)
  {
    var yArray = [];
    for(var y = 0; y < LOGICAL_HEIGHT; y++)
    {
      yArray[y] = BLANK;
    }
    logicalScreen[x] = yArray;
  }
}

function setupInput()
{
	document.addEventListener('keydown', function (key) {
    keysDown.set(key.code, true);
	});
	
	document.addEventListener('keyup', function (key) {
    keysDown.set(key.code, false);
	});
}

//-------------------
// Logical Screen drawing functions
//-------------------

function clearScreen()
{
  for(var x = 0; x < LOGICAL_WIDTH; x++)
  {
    var yArray = [];
    for(var y = 0; y < LOGICAL_HEIGHT; y++)
    {
      yArray[y] = BLANK;
    }
    logicalScreen[x] = yArray;
  }
}

function drawNoise()
{
  for(var x = 0; x < LOGICAL_WIDTH; x++)
  {
    var yArray = [];
    for(var y = 0; y < LOGICAL_HEIGHT; y++)
    {
      yArray[y] = BLANK;
      if(Math.random() < 0.1)
      {
        yArray[y] = Math.floor(Math.random() * 3 + 1);
      }
    }
    logicalScreen[x] = yArray;
  }
}

function drawLine(x1, y1, x2, y2, color)
{ 
  y1 = LOGICAL_HEIGHT - y1;
  y2 = LOGICAL_HEIGHT - y2;
  
  if(x1 >= LOGICAL_WIDTH || x2 >= LOGICAL_WIDTH || x1 < 0 || x2 < 0)
    return;
  
  if(y1 >= LOGICAL_HEIGHT || y2 >= LOGICAL_HEIGHT || y1 < 0 || y2 < 0)
    return;
  
  var isYInverted = y2 < y1;
  var isXInverted = x2 < x1;
  
  if(color === undefined)
  {
    color = BLUE;
  }
  
  // If it's a straight line, just draw it using a for loop.
  if(x1 === x2 || y1 === y2)
  {
    // Flip x and y if either are not in ascending order
    if(isXInverted)
    {
      var t = x2;
      x2 = x1;
      x1 = t;
    }

    if(isYInverted)
    {
      var t = y2;
      y2 = y1;
      y1 = t;
    }
    
    for(var x = x1; x <= x2; x++)
    {
      for(var y = y1; y <= y2; y++)
      {
        logicalScreen[x][y] = color;
      }
    }
    
    return;
  }

  // y = mx + c method
  if(isXInverted)
  { 
    var tx = x2;
    x2 = x1;
    x1 = tx;

    var ty = y2;
    y2 = y1;
    y1 = ty;
  }
  
  var slope = (y2 - y1) / (x2 - x1);
  var c = y1 - (slope * x1);
  
  if(Math.abs(slope) >= 1.0) 
  {
    var yStart = y1;
    var yEnd = y2;
    if(y1 > y2)
    {
      yStart = y2;
      yEnd = y1;
    }
    
    for(var y = yStart; y <= yEnd; y++)
    {
      var x = (y - c) / slope;
      logicalScreen[Math.round(x)][y] = color;
    }
  }
  else
  {
    for(var x = x1; x <= x2; x++)
    {
      var y = slope * x + c;
      logicalScreen[x][Math.round(y)] = color;
    }
  }
  
  logicalScreen[x1][y1] = color;
  logicalScreen[x2][y2] = color;
}

function drawCharacter(x, y, character, color)
{
  if(color === undefined)
  {
     color = BLUE;
  }
  
  if(x < 0 || x >= LOGICAL_WIDTH - 4)
    return;
  
  if(y < 0 || y >= LOGICAL_HEIGHT - 5)
    return;
  
  for(var i = 0; i < 3; i++)
  {
    for(var j = 0; j < 5; j++)
    { 
      if(letters[character][j][i] === 1)
      {
        logicalScreen[x + i][y + j] = color;
      }
      else
      {
        logicalScreen[x + i][y + j] = BLANK;
      }
    }
  }
}

function drawString(y, x, str, color)
{
  str = str + "";
  
  var renderOffset = 0;
  for(var i = 0; i < str.length; i++)
  {
    drawCharacter(x + renderOffset, y, str.charAt(i), color);
    if(str.charAt(i) !== " ")
    {
       renderOffset += 4;
    }
    else if(str.charAt(i) === " ")
    {
       renderOffset += 4;
    }
  }
}

//----------------
// Scene subroutines
//----------------

function doIntroScene()
{ 
  if(currentSubstate === "initializing")
  {
    drawString(1, 1, "welcome to knight's quest");
    drawString(10, 1, "press exe to continue");
    
    if(getCookie('saveDataExists'))
    {
      drawString(57, 1, "1:new game");
      drawString(57, 87, "2:continue");
    }

    currentSubstate = "waiting-first-page-confirm";
    addDelay();
    return;
  }
  
  if(currentSubstate === "waiting-first-page-confirm")
  {
    if(keysDown.get("Enter") || keysDown.get("Digit2"))
    { 
      if(getCookie('saveDataExists'))
      {
        loadVariables();
        changeScene("world-map");
        return;
      }
      else
      {
        initializeVariables();
        currentSubstate = "chose-new-game";
      }
    }
    
    if(keysDown.get("Digit1"))
    { 
      initializeVariables();
      currentSubstate = "chose-new-game";
    }
    
    if(currentSubstate === "chose-new-game")
    {
      initializeVariables();
      
      clearScreen();
      drawString(1,1, "instructions:");
      drawString(10,1, "press the corresponding number");
      drawString(20,1, "on the screen to make a choice,");
      drawString(30,1, "and '0' to view status when");
      drawString(40,1, "on the map.");
      drawString(50,1, "good luck!");
      
      currentSubstate = "waiting-second-page-confirm";
      addDelay();
    }
  }
  
  if(currentSubstate === "waiting-second-page-confirm")
  {
    if(keysDown.get("Enter"))
    { 
      clearScreen();
      drawString(1,1,"you arrive in the troubled");
      drawString(8,1,"kingdom of vale, where an evil");
      drawString(15,1,"tyrant rules with the brute");
      drawString(22,1,"force of his army. a local man");
      drawString(29,1,"tells you that the tyrant lives");
      drawString(36,1,"in the castle, which is");
      drawString(43,1,"protected by a power that can");
      drawString(50,1,"be broken only with three");
      drawString(57,1,"different keys.");
      
      currentSubstate = "waiting-third-page-confirm";
      addDelay();
      return;
    }
  }
  
  if(currentSubstate === "waiting-third-page-confirm")
  {
    if(keysDown.get("Enter"))
    { 
      clearScreen();
      
      drawString(1,1,"upon saying this, he hands you");
      drawString(8,1,"a blue key and hurries");
      drawString(15,1,"away, leaving you in the");
      drawString(22,1,"centre of the city.");
      drawString(29,1,"being the generous man that");
      drawString(36,1,"you are, you decide to free");
      drawString(43,1,"this kindom and its");
      drawString(50,1,"citizens...");
      
      currentSubstate = "waiting-fourth-page-confirm";
      addDelay();
      return;
    }
  }
  
  if(currentSubstate === "waiting-fourth-page-confirm")
  {
    if(keysDown.get("Enter"))
    { 
      changeScene("world-map");
      return;
    }
  }
}

function doWorldMapScene()
{
  if(currentSubstate === "initializing")
  {
    clearScreen();
    drawLine(1,2,126,2);
    drawLine(1,2,1,63);
    drawLine(1,63,126,63);
    drawLine(126,2,126,63);
    drawLine(5,37,5,52);
    drawLine(5,52,35,52);
    drawLine(35,52,35,37);
    drawLine(5,37,35,37);
    drawLine(4,51,13,60);
    drawLine(13,60,29,60);
    drawLine(29,60,36,51);
    drawLine(31,57,31,60);
    drawLine(31,60,34,60);
    drawLine(34,60,34,54);
    drawString(6,15,"inn");
    drawLine(17,37,17,45);
    drawLine(17,45,23,45);
    drawLine(23,45,23,37);
    drawLine(21,41,21,41);
    drawLine(8,42,8,48);
    drawLine(8,48,14,48);
    drawLine(14,48,14,42);
    drawLine(14,42,8,42);
    drawLine(11,42,11,48);
    drawLine(8,45,14,45);
    drawLine(26,42,26,48);
    drawLine(26,48,32,48);
    drawLine(32,48,32,42);
    drawLine(32,42,26,42);
    drawLine(29,42,29,48);
    drawLine(26,45,32,45);
    drawLine(120,37,90,37);
    drawLine(90,37,90,52);
    drawLine(90,52,120,52);
    drawLine(120,52,120,37);
    drawLine(89,51,95,60);
    drawLine(95,60,115,60);
    drawLine(115,60,121,51);
    drawLine(102,37,102,45);
    drawLine(102,45,108,45);
    drawLine(108,45,108,37);
    drawLine(106,41,106,41);
    drawString(6,98,"shop");
    drawLine(90,5,90,27);
    drawLine(90,27,120,27);
    drawLine(120,27,120,5);
    drawLine(120,5,90,5);
    drawLine(99,14,111,14);
    drawLine(111,14,111,5);
    drawLine(105,14,105,5);
    drawLine(99,14,99,5);
    drawLine(100,19,106,19);
    drawLine(106,19,106,25);
    drawLine(106,25,100,25);
    drawLine(100,25,100,19);
    drawLine(106,22,110,22);
    drawLine(110,22,110,16);
    drawLine(110,16,104,16);
    drawLine(104,16,104,19);
    drawLine(102,23,102,23);
    drawLine(104,23,104,23);
    drawLine(104,21,104,21);
    drawLine(102,21,102,21);
    drawLine(108,18,108,18);
    drawLine(20,30,5,4);
    drawLine(20,30,35,4);
    drawLine(20,30,21,4);
    drawLine(20,30,19,4);
    drawLine(50,20,50,47);
    drawLine(50,47,55,47);
    drawLine(55,47,55,42);
    drawLine(55,42,60,42);
    drawLine(60,42,60,47);
    drawLine(60,47,65,47);
    drawLine(65,47,65,42);
    drawLine(65,42,70,42);
    drawLine(70,42,70,47);
    drawLine(70,47,75,47);
    drawLine(75,47,75,20);
    drawLine(75,20,50,20);
    drawLine(57,20,57,34);
    drawLine(57,34,69,34);
    drawLine(69,34,69,20);
    drawLine(60,20,60,34);
    drawLine(63,20,63,34);
    drawLine(66,20,66,34);
    drawLine(57,30,69,30);
    drawLine(57,26,69,26);
    drawLine(57,23,69,23);
    drawString(3,3,"1");
    drawString(56,3,"3");
    drawString(3,122,"2");
    drawString(56,122,"4");
    drawString(10,61,"5");
    drawString(56,50,"0:stats");

    currentSubstate = "waiting-choice";
    addDelay();
    return;
  }
  
  if(currentSubstate === "waiting-choice")
  {
    if(keysDown.get("Digit1"))
    {
      changeScene("inn");
    }
    
    if(keysDown.get("Digit2"))
    {
      changeScene("shop");
    }
    
    if(keysDown.get("Digit3"))
    {
      changeScene("battle");
    }
    
    if(keysDown.get("Digit4"))
    {
      changeScene("casino");
    }
    
    if(keysDown.get("Digit5"))
    {
      changeScene("final-battle");
    }
    
    if(keysDown.get("Digit0"))
    {
      changeScene("stats-screen");
    }
  }
}

function doInnScene()
{
  if(currentSubstate === "initializing")
  {
    clearScreen();
    
    drawLine(1,2,126,2);
    drawLine(1,63,126,63);
    drawLine(1,2,1,63);
    drawLine(126,2,126,63);
    drawLine(1,25,126,25);
    drawLine(1,50,25,50);
    drawLine(25,50,25,25);
    drawLine(4,46,21,46);
    drawLine(4,46,4,25);
    drawLine(21,46,21,25);
    drawLine(100,40,100,58);
    drawLine(100,58,120,58);
    drawLine(120,58,120,40);
    drawLine(100,40,120,40);
    drawLine(97,37,97,61);
    drawLine(97,37,123,37);
    drawLine(97,61,123,61);
    drawLine(123,37,123,61);
    drawLine(110,40,110,58);
    drawLine(100,49,120,49);
    drawLine(45,25,45,28);
    drawLine(75,25,75,28);
    drawLine(45,28,58,36);
    drawLine(75,28,62,36);
    drawLine(58,36,58,38);
    drawLine(62,36,62,38);
    drawLine(57,38,63,38);
    drawLine(57,38,53,42);
    drawLine(63,38,67,42);
    drawLine(53,42,53,51);
    drawLine(67,42,67,51);
    drawLine(53,51,67,51);
    drawLine(53,51,53,53);
    drawLine(67,51,67,53);
    drawLine(53,53,67,53);
    drawLine(67,53,65,57);
    drawLine(53,53,55,57);
    drawLine(55,57,65,57);
    drawLine(56,48,58,48);
    drawLine(61,48,63,48);
    drawLine(60,46,58,44);
    drawLine(58,44,60,44);
    drawLine(57,42,63,42);
    drawLine(61,40,58,40);
    drawLine(28,19,87,19);
    drawLine(28,19,28,10);
    drawLine(87,19,87,10);
    drawLine(28,10,87,10);
    drawString(48,30,'one night: 10g');
    drawString(7,3,'1.stay');
    drawString(55,98,'2.leave');
    
    currentSubstate = "waiting-choice";
    addDelay();
    return;
  }
  
  if(currentSubstate === "waiting-choice")
  {
    if(keysDown.get("Digit2"))
    {
      clearScreen();
      
      drawString(1,1, "you leave the inn.");
      
      currentSubstate = "left-inn";
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit1"))
    {
      clearScreen();
      
      if(gold < 10)
      {
        drawString(1,1,'you turn out your pockets,');
        drawString(8,1,'but you cannot find enough');
        drawString(15,1,'gold to pay. still unrested,');
        drawString(22,1,'you stagger out of the inn.');
        
        currentSubstate = "left-inn";
        addDelay();
        return;
      }
      else 
      {
        drawString(1,1,'you wake the next morning');
        drawString(8,1,'feeling perfectly healthy.');
        drawString(15,1,'you thank the innkeeper and');
        drawString(22,1,'leave.');
        
        gold -= 10;
        fullRestore();
        
        currentSubstate = "left-inn";
        addDelay();
        return;
      }
    }
  }
  
  if(currentSubstate === "left-inn")
  {
    if(keysDown.get("Enter"))
    { 
      changeScene("world-map");
      return;
    }
  }
}

function doShopScene()
{
  if(currentSubstate === "initializing")
  {
    clearScreen();
    
    drawString(1,45,'vale shop');
    drawLine(44,57,80,57);
    drawString(10,1,'1. knife 30g');
    drawString(18,1,'2. hatchet 200g');
    drawString(26,1,'3. sword 500g');
    drawString(34,1,'4. s.shield 50g');
    drawString(42,1,'5. l.shield 150g');
    drawString(50,1,'6. potion 15g');
    drawString(58,1,'7. green key 400g',GREEN);
    drawString(58,95,'8. leave');
    drawString(18,80,'your funds:');
    drawString(26,90,gold);
    drawString(26,105,'g');
    
    currentSubstate = "waiting-choice";
    addDelay();
    return;
  }
  
  if(currentSubstate === "waiting-choice")
  {
    if(keysDown.get("Digit8"))
    {
      clearScreen();
      
      drawString(1,1,"you leave the shop.");
      
      currentSubstate = "left-shop";
      return;
    }
    
    if(keysDown.get("Digit1"))
    {
      clearScreen();
      
      if(knife >= 1)
      {
        drawString(1,1,'you have no use for that.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      if(gold < 30)
      {
        drawString(1,1,'you do not have enough gold.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      gold -= 30;
      knife = 1;
      drawString(1,1,'you buy a knife.');
      currentSubstate = "purchase-attempted";
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit2"))
    {
      clearScreen();
      
      if(knife >= 2)
      {
        drawString(1,1,'you have no use for that.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      if(gold < 200)
      {
        drawString(1,1,'you do not have enough gold.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      gold -= 200;
      knife = 2;
      drawString(1,1,'you buy a hatchet.');
      currentSubstate = "purchase-attempted";
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit3"))
    {
      clearScreen();
      
      if(knife >= 4)
      {
        drawString(1,1,'you have no use for that.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      if(gold < 500)
      {
        drawString(1,1,'you do not have enough gold.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      gold -= 500;
      knife = 4;
      drawString(1,1,'you buy a sword.');
      currentSubstate = "purchase-attempted";
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit4"))
    {
      clearScreen();
      
      if(shield >= 1)
      {
        drawString(1,1,'you have no use for that.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      if(gold < 50)
      {
        drawString(1,1,'you do not have enough gold.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      gold -= 50;
      shield = 1;
      drawString(1,1,'you buy a small shield.');
      currentSubstate = "purchase-attempted";
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit5"))
    {
      clearScreen();
      
      if(shield >= 2)
      {
        drawString(1,1,'you have no use for that.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      if(gold < 150)
      {
        drawString(1,1,'you do not have enough gold.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      gold -= 150;
      shield = 2;
      drawString(1,1,'you buy a large shield.');
      currentSubstate = "purchase-attempted";
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit6"))
    {
      clearScreen();
      
      if(potions >= 20)
      {
        drawString(1,1,'you have no use for that.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      if(gold < 15)
      {
        drawString(1,1,'you do not have enough gold.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      gold -= 15;
      potions += 1;
      drawString(1,1,'you buy a potion.');
      currentSubstate = "purchase-attempted";
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit7"))
    {
      clearScreen();
      
      if(greenKey === 462)
      {
        drawString(1,1,'you have no use for that.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      if(gold < 400)
      {
        drawString(1,1,'you do not have enough gold.');
        currentSubstate = "purchase-attempted";
        addDelay();
        return;
      }
      
      gold -= 400;
      greenKey = 462;
      drawString(1,1,'you buy a green key.');
      currentSubstate = "purchase-attempted";
      addDelay();
      return;
    }
  }
  
  if(currentSubstate === "left-shop")
  {
    if(keysDown.get("Enter"))
    { 
      changeScene("world-map");
      return;
    }
  }
  
  if(currentSubstate === "purchase-attempted")
  {
    if(keysDown.get("Enter"))
    { 
      changeScene("shop");
      return;
    }
  }
}

function doCasinoScene()
{
  if(currentSubstate === "initializing")
  {
    clearScreen();
    
    drawString(1,1,"casino");
    
    currentSubstate = "leave-casino";
    addDelay();
    return;
  }
  
  if(currentSubstate === "leave-casino")
  {
    if(keysDown.get("Enter"))
    {
      changeScene("world-map");
      return;
    }
  }
}

function doBattleScene()
{
  if(currentSubstate === "initializing")
  {
    clearScreen();
    
    isFinalBattle = 0;
    attack = level + 9 + knife;
    defense = level + 4 + shield;
    opponentLevel = Math.floor(Math.random() * 3) + level - 1;
    if(opponentLevel === 0)
      opponentLevel = 1;
    
    opponentHealth = 2 * opponentLevel + 18;
    
    if(orangeKey === 462)
    {
      opponentHasKey = false;
    }
    else if(level >= 15)
    {
      opponentHasKey = true;
    }
    else if(level === 1)
    {
      opponentHasKey = false;
    }
    else
    {
      var r = Math.floor(Math.random() * (16 - level)) + 1;
      opponentHasKey = r === 1;
    }

    if(opponentHasKey)
    {
      drawString(1,1,"upon entering the soldiers'");
      drawString(8,1,"camp, you see that one of the");
      drawString(15,1,"soldiers is holding an orange");
      drawString(22,1,"key. you ask him if you can");
      drawString(29,1,"have it, but he doesn't like");
      drawString(36,1,"the idea...");
    }
    else
    {
      drawString(1,1,"upon entering the soldiers'");
      drawString(8,1,"camp, you approach a soldier");
      drawString(15,1,"and ask about a key. the");
      drawString(22,1,"soldier tells you to get lost.");
      drawString(29,1,"you decide to teach him some");
      drawString(36,1,"manners...");
    }
    
    currentSubstate = "wait-initial-confirmation";
    addDelay();
    return;
  }
  
  if(currentSubstate === "wait-initial-confirmation")
  {
    if(keysDown.get("Enter"))
    {
      currentSubstate = "battle-initial-render";
    }
    return;
  }
  
  if(currentSubstate === "battle-initial-render")
  {
    clearScreen();
    
    drawLine(1,2,1,63);
    drawLine(1,63,126,63);
    drawLine(126,63,126,2);
    drawLine(126,2,1,2);
    drawLine(1,20,126,20);
    drawString(47,11,'1. attack');
    drawString(55,11,'2. special');
    drawString(47,79,'3. potion');
    drawString(55,79,'4. flee');
    drawLine(64,63,64,20);
    drawLine(32,63,32,20);
    drawLine(96,63,96,20);
    drawString(4,4,'lvl:');
    drawString(4,20,level);
    drawString(14,4,'hp:');
    drawString(14,20,health);
    drawString(24,4,'pp:');
    drawString(24,20,mana);

    if(opponentLevel >= 1 && opponentLevel <= 3)
    {
      drawString(4,99,'recruit');
    }

    if(opponentLevel >= 4 && opponentLevel <= 6)
    {
      drawString(4,99,'soldier');
    }

    if(opponentLevel >= 7 && opponentLevel <= 9)
    {
      drawString(4,99,'captain');
    }

    if(opponentLevel >= 10 && opponentLevel <= 12)
    {
      drawString(4,99,'major');
    }

    if(opponentLevel >= 13 && !isFinalBattle)
    {
      drawString(4,98,'general');
    }
    
    if(isFinalBattle)
    {
      drawString(4,99,'tyrant');
    }

    drawString(14,99,'lvl:');
    if(!isFinalBattle)
    {
      drawString(14,117,opponentLevel);
    }
    else
    {
      drawString(14,117,"18");
    }
    drawString(24,99,'hp:');
    drawString(24,117,opponentHealth);
    drawLine(96,31,126,31);
    drawLine(1,31,32,31);
    
    drawLine(42,23,45,23);  // player
    drawLine(50,23,53,23);
    drawLine(42,23,46,33); 
    drawLine(46,33,50,23)
    drawLine(46,33,46,46);
    drawLine(46,46,44,48);
    drawLine(46,46,48,48);
    drawLine(48,48,48,51);
    drawLine(44,48,44,51);
    drawLine(44,51,48,51);
    drawLine(48,52,48,52);
    drawLine(44,52,44,52);
    drawLine(45,53,47,53);
    drawLine(46,42,49,39);
    drawLine(49,39,52,43);
    drawLine(46,42,43,39);
    drawLine(43,39,40,39);

    if(knife === 1)
    {
      drawLine(40,38,40,43);
      drawLine(39,40,41,40);
    }

    if(knife === 2)
    {
      drawLine(40,38,40,46);
      drawLine(40,44,42,46);
      drawLine(40,43,42,41);
      drawLine(42,46,42,41);
    }

    if(knife === 4)
    {
      drawLine(40,38,40,48);
      drawLine(39,41,41,41);
    }

    if(shield === 1)
    {
      drawLine(52,39,52,45);
    }

    if(shield === 2)
    {
      drawLine(52,36,52,49);
    }
    
    if(!isFinalBattle)
    {
      drawLine(75,23,78,23);
      drawLine(83,23,86,23);
      drawLine(78,23,82,33);
      drawLine(86,23,82,33);
      drawLine(82,33,82,46);
      drawLine(82,46,80,48);
      drawLine(82,46,84,48);
      drawLine(80,48,80,53);
      drawLine(84,48,84,53);
      drawLine(80,53,84,53);
      drawLine(82,42,79,39);
      drawLine(82,40,79,37);
      drawLine(79,39,74,42);
      drawLine(79,37,74,42);

      if(opponentLevel >= 1 && opponentLevel <= 3)
      {
        drawLine(74,40,74,45);
        drawLine(73,42,75,42);
      }

      if(opponentLevel >= 4 && opponentLevel <= 6)
      {
        drawLine(75,41,75,49);
        drawLine(75,47,73,49);
        drawLine(75,46,73,44);
        drawLine(73,49,73,44);
      }

      if(opponentLevel >= 7 && opponentLevel <= 9)
      {
        drawLine(74,40,74,50);
        drawLine(73,42,75,42);
      }

      if(opponentLevel >= 10 && opponentLevel <= 12)
      { 
        drawLine(89,27,70,46);
        drawLine(70,46,68,46);
        drawLine(68,46,68,48);
        drawLine(68,48,70,48);
        drawLine(70,48,70,46);
      }

      if(opponentLevel >= 13)
      {
        drawLine(74,37,74,54);
        drawLine(74,50,71,54);
        drawLine(74,49,71,45);
        drawLine(71,54,71,45);
      }
    }
    else
    {
      drawLine(83,23,86,23);
      drawLine(91,23,94,23);
      drawLine(86,23,90,33);
      drawLine(94,23,90,33);
      drawLine(90,33,90,46);
      drawLine(90,46,88,48);
      drawLine(90,46,92,48);
      drawLine(88,48,88,53);
      drawLine(92,48,92,53);
      drawLine(88,53,92,53);
      drawLine(90,42,87,39);
      drawLine(90,40,87,37);
      drawLine(87,39,82,42);
      drawLine(87,37,82,42);
      drawLine(93,29,74,53);
      drawLine(92,29,73,53);
      drawLine(75,51,69,51);
      drawLine(75,51,75,57);
      drawLine(78,46,78,40);
      drawLine(78,46,84,46);
      drawLine(69,51,78,40);
      drawLine(75,57,84,46);
    }
    
    currentSubstate = "wait-player-choice";
    addDelay();
    return;
  }
  
  if(currentSubstate === "wait-player-choice")
  {
    drawString(36,3,"       ");
    drawString(36,98,"       ");
    
    if(keysDown.get("Digit1"))
    {
      drawString(36,3,"attack!");
      
      
      var damage = Math.floor(Math.random() * 3 + attack - 1 - opponentLevel - 4);
      if(damage < 1)
      {
        damage = 1;
      }
      
      opponentHealth -= damage;
      if(opponentHealth < 0)
      {
        opponentHealth = 0;
      }
      
      drawString(24,117,"  ");
      drawString(24,117,opponentHealth);
      
      if(opponentHealth === 0)
      {
        currentSubstate = "battle-victory";
        addDelay();
        return;
      }
      
      currentSubstate = "enemy-turn";
      
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit2"))
    {
      if(mana < 3)
        return;
      
      drawString(36,3,"special");
      
      var damage = Math.floor(1.5 * attack - 1 - opponentLevel - 4);
      if(damage < 1)
      {
        damage = 1;
      }
      
      mana -= 3;
      drawString(24,20,"  ");
      drawString(24,20,mana);
      
      opponentHealth -= damage;
      if(opponentHealth < 0)
      {
        opponentHealth = 0;
      }
      
      drawString(24,117,"  ");
      drawString(24,117,opponentHealth);
      
      if(opponentHealth === 0)
      {
        currentSubstate = "battle-victory";
        addDelay();
        return;
      }
      
      currentSubstate = "enemy-turn";
      
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit3"))
    {
      if(potions <= 0)
      {
        addDelay();
        return;
      }

      drawString(36,3,"potion");
      
      potions -= 1;
      health += 10;
      if(health > 2 * level + 18)
      {
        health = 2 * level + 18;
      }
      drawString(14,20,"  ");
      drawString(14,20,health);
      
      currentSubstate = "enemy-turn";
      
      addDelay();
      return;
    }
    
    if(keysDown.get("Digit4"))
    {
      drawString(36,3,"flee");
      
      currentSubstate = "battle-flee";
      addDelay();
      return;
    }
  }
  
  if(currentSubstate === 'enemy-turn')
  {
    if(opponentHealth === 0)
      return;
    
    drawString(36,98,"       ");
    drawString(36,3,"       ");
    drawString(36,98,"attack!");
       
    var damage = Math.floor(Math.random() * 3 + opponentLevel + 8 - defense);
    if(damage < 1)
    {
      damage = 1;
    }
    
    health -= damage;
    if(health < 0)
    {
      health = 0;
    }
    
    drawString(14,20,"  ");
    drawString(14,20,health);
    
    if(health === 0)
    {
      currentSubstate = "battle-defeat";
      addDelay();
      return;
    }
    
    currentSubstate = "wait-player-choice";
    
    addDelay();
    return;
  }
  
  if(currentSubstate === "battle-flee")
  {
    clearScreen();
    
    drawString(1,1,"you decide that the smartest");
    drawString(8,1,"decision is to run away...");
    
    currentSubstate = "battle-over";
    addDelay();
    return;
  }
  
  if(currentSubstate === "battle-defeat")
  {
    clearScreen();
    
    drawString(1,1,"giving in to your terrible");
    drawString(8,1,"injuries and exhaustion,");
    drawString(15,1,"you collapse...");
    
    var r = Math.random();
    
    if(r < 0.9)
    {
      gold = 0;
      drawString(29,1,"you lose all your gold!");
    }
    if(r < 0.4 && knife > 0)
    {
      knife = 0;
      drawString(36,1,"and your weapon!");
    }
    if(r < 0.1 && shield > 0)
    {
      shield = 0;
      drawString(43,1,"finally, you lose your shield!");
    }
    
    fullRestore();
    
    currentSubstate = "battle-over";
    addDelay();
    return;
  }
  
  if(currentSubstate === "battle-victory")
  {
    if(isFinalBattle)
    {
      changeScene("final-cutscene");
      return;
    }
    
    clearScreen();
    
    var expGained = 10 * opponentLevel + 10;
    var goldGained = 10 * opponentLevel + 20;
    
    experience += expGained;
    if(experience >= 25 * level * level + 75 * level)
    {
      level += 1;
      drawString(29,1,"level up!");
    }
    
    gold += goldGained;
    
    drawString(1,1,"you defeated the enemy!");
    drawString(8,1,"you gain     experience");
    drawString(8,37,expGained);
    drawString(15,1, "and find     gold.");
    drawString(15,37,goldGained);
    
    if(opponentHasKey)
    {
      orangeKey = 462;
      drawString(43,1,"the soldier dropped an orange");
      drawString(50,1,"key!");
    }
    
    currentSubstate = "battle-over";
    addDelay();
    return;
  }
  
  if(currentSubstate === "battle-over")
  {
    if(keysDown.get("Enter"))
    {
      changeScene("world-map");
      return;
    }
  }
}

function doFinalBattleScene()
{
  if(currentSubstate === "initializing")
  {
    clearScreen();
    
    if(orangeKey !== 462 || greenKey != 462)
    {
      drawString(1,1,"an invisible force prevents");
      drawString(8,1,"you entering the castle. you");
      drawString(15,1,"don't have all three keys!");
      
      currentSubstate = "battle-over";
      addDelay();
      return;
    }
    else
    {
      drawString(1,1,'you enter the castle,');
      drawString(8,1,'intending to talk to the');
      drawString(15,1,'misguided dictator. however,');
      drawString(22,1,'as soon as you approach him,');
      drawString(29,1,'he attacks! left with no time');
      drawString(36,1,'for negotiation, you also');
      drawString(43,1,'raise your weapon...');
      
      isFinalBattle = 1;
      currentScene = "battle";
      currentSubstate = "wait-initial-confirmation";
      
      attack = level + 9 + knife;
      defense = level + 4 + shield;
      opponentLevel = 20;
      opponentHealth = 2 * opponentLevel + 18;
      return;
    } 
  }
  
  if(currentSubstate === "battle-over")
  {
    if(keysDown.get("Enter"))
    {
      changeScene("world-map");
      return;
    }
  }
}

function doStatsScene()
{
  if(currentSubstate === "initializing")
  {    
    clearScreen();
    drawLine(1,2,1,63);
    drawLine(1,2,126,2);
    drawLine(1,63,126,63);
    drawLine(126,63,126,2);
    drawString(3,4,"level:");
    drawString(3,38,level);
    drawString(9,4,"health:");
    drawString(9,38,health);
    drawString(9,50,"/");
    drawString(9,58,(2 * level + 18));
    drawString(15,4,"power:");
    drawString(15,38,mana);
    drawString(15,50,"/");
    drawString(15,58,(2 * level +8));
    drawString(21,4,"attack:");
    attack = (level + 9 + knife);
    drawString(21,38,attack);
    drawString(27,4,"defense:");
    defense = (level + 4 + shield);
    drawString(27,38,defense);
    drawString(33,4,"potions:");
    drawString(33,38,potions);
    drawString(39,4,"gold:");
    drawString(39,38,gold);
    drawString(47,4,"equipment:");

    if(knife === 1)
    {
      drawString(47,50,"knife");
    }

    if(knife === 2)
    {
      drawString(47,50,"hatchet");
    }

    if (knife === 4)
    {
      drawString(47,50,"sword");
    }

    if (!knife)
    {
      drawString(47,50,"none");
    }

    drawString(47,80,"/");

    if(shield === 1)
    {
      drawString(47,86,"s. shield");
    }

    if(shield === 2)
    {
      drawString(47,86,"l. shield");
    }

    if(!shield)
    {
      drawString(47,86,"none");
    }

    drawString(55,4,"experience:");
    drawString(55,50,experience);
    drawString(55,80,"/");
    drawString(55,86,25 * level * level + 75 * level);
    drawLine(75,61,124,61);
    drawLine(75,20,75,61);
    drawLine(75,20,124,20);
    drawLine(124,20,124,61);
    drawString(6,90,"keys:");
    drawLine(89,52,109,52);
    drawString(16,83,"blue key");

    if (greenKey === 462)
    {
      drawString(26,82,"green key",GREEN);
    }

    if (orangeKey === 462)
    {
      drawString(36,80,"orange key",ORANGE);
    }
        
    currentSubstate = "waiting";
    addDelay();
    return;
  }
  
  if(currentSubstate === "waiting")
  {
    if(keysDown.get("Enter"))
    {
      changeScene("world-map");
      return;
    }
  }
}

function doFinalCutsceneScene()
{
  if(currentSubstate === "initializing")
  {
    clearScreen();
    
    drawString(1,1,"the tyrant finally surrenders,");
    drawString(8,1,"and you drag him to the city's");
    drawString(15,1,"prison.");
    drawString(29,1,"he goes quietly.");
    
    currentSubstate = "final-cutscene-2";
    addDelay();
    return;
  }
  
  if(currentSubstate === "final-cutscene-2")
  {
    if(keysDown.get("Enter"))
    {
      clearScreen();
      
      drawString(1,1,"the citizens of the kingdom");
      drawString(8,1,"consider you a hero. they even");
      drawString(15,1,"ask you to become the new");
      drawString(22,1,"king. however, you turn down");
      drawString(29,1,"the offer. you have other");
      drawString(36,1,"kingdoms to save...");
      
      currentSubstate = "final-cutscene-3";
      addDelay();
      return;
    }
  }
  
  if(currentSubstate === "final-cutscene-3")
  {
    if(keysDown.get("Enter"))
    {
      clearScreen();
      
      drawLine(1,2,1,63);
      drawLine(1,63,126,63);
      drawLine(126,63,126,2);
      drawLine(126,2,1,2);
      drawLine(1,42,126,42);
      drawLine(60,42,50,12);
      drawLine(66,42,76,12);
      drawLine(1,4,15,4);
      drawLine(15,4,15,9);
      drawLine(15,9,35,9);
      drawLine(35,9,35,4);
      drawLine(35,4,50,4);
      drawLine(50,4,50,9);
      drawLine(50,9,75,9);
      drawLine(75,9,75,4);
      drawLine(75,4,90,4);
      drawLine(90,4,90,9);
      drawLine(90,9,110,9);
      drawLine(110,9,110,4);
      drawLine(110,4,125,4);
      drawLine(62,43,63,44);
      drawLine(64,43,63,44);
      drawLine(63,44,63,48);
      drawLine(62,46,64,46);
      drawLine(63,47,62,48);
      drawLine(64,48,64,50);
      drawLine(62,48,62,50);
      drawLine(62,50,64,50);
      
      currentSubstate = "final-cutscene-4";
      addDelay();
      return;
    };
  }
  
  if(currentSubstate === "final-cutscene-4")
  {
    if(keysDown.get("Enter"))
    {
      clearScreen();
      
      drawString(1,1,'credits:');
      drawString(15,1,'graphics designer: sp');
      drawString(22,1,'testers: lc hs ap wg jbg sp dk');
      drawString(29,1,'special thanks: ac');
      drawString(36,1,'programmer and producer: ag');
      drawString(50,1,'this program is free for');
      drawString(57,1,'personal use. final version.');
      
      currentSubstate = "final-cutscene-5";
      initializeVariables();
      addDelay();
      return;
    };
  }
}
//----------------
// Game Loop functions
//----------------

function addDelay(time)
{ 
  if(time === undefined)
    time = 30;

  delay = time;
}

function changeScene(scene)
{
  currentScene = scene;
  currentSubstate = "initializing";
  saveVariables();
}

function doGameLogic()
{
  if(delay > 0)
  {
     delay--;
     return;
  }
  
  if(currentScene === "intro")
  {
    doIntroScene();
  }
  
  if(currentScene === "world-map")
  {
    doWorldMapScene();
  }
  
  if(currentScene === "stats-screen")
  {
    doStatsScene();
  }
  
  if(currentScene === "inn")
  {
    doInnScene();
  }
  
  if(currentScene === "shop")
  {
    doShopScene();
  }
  
  if(currentScene === "battle")
  {
    doBattleScene();
  }
  
  if(currentScene === "final-battle")
  {
    doFinalBattleScene();
  }
  
  if(currentScene === "final-cutscene")
  {
    doFinalCutsceneScene();
  }
  
  if(currentScene === "casino")
  {
    doCasinoScene();
  }
}

function drawFrame()
{
  context.clearRect(0, 0, canvas.width, canvas.height);
	context.fillStyle = "#c8ddbe";
	context.fillRect(0, 0, canvas.width, canvas.height);
	
  for(var lx = 0; lx < LOGICAL_WIDTH; lx++)
  {
    for(var ly = 0; ly < LOGICAL_HEIGHT; ly++)
    {
      var colorType = logicalScreen[lx][ly];
      var backgroundColor = "#c8ddbe";
      var foregroundColor = "#c8ddbe";
      
      if(colorType === GREEN)
      {
        foregroundColor = "#4fa55a";
        backgroundColor = "#67b06e";
      }
      if(colorType === ORANGE)
      {
        foregroundColor = "#cc7863"; 
        backgroundColor = "#cb8c75";
      }
      if(colorType === BLUE)
      {
        foregroundColor = "#525c89";  
        backgroundColor = "#6a7694";
      }
      if(colorType === DEBUG)
      {
        foregroundColor = "#ff0000";  
        backgroundColor = "#ff0000";
      }
      
      if(colorType !== BLANK)
      {
        context.fillStyle = backgroundColor;
        context.fillRect(lx * 5, ly * 5, 5, 5);
        context.fillStyle = foregroundColor;
        context.fillRect(lx * 5 + 1, ly * 5 + 1, 3, 3);
      }
    }
  }
  
}

function globalGameLoopTick()
{
  doGameLogic();
  drawFrame();
  
  globalTick++;
  requestAnimationFrame(globalGameLoopTick);
}

//-------
// Main start
//-------

function getGameStarted()
{
  setupDisplayScreen();
  setupLogicalScreen();
  setupInput();
  
  window.requestAnimationFrame(globalGameLoopTick);
}

window.addEventListener("load", function()
{  
  getGameStarted();
});








