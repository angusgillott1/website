function addMenu()
{
    const menu = document.createElement("div");
    menu.id = "global-menu";

    const html = `
        <div id="home-button" style="position: fixed; top: 12px; left: 12px; cursor: pointer" class="hide-when-launched">
            <img src="/images/icons/home-24.svg" alt="" width="24" height="24">
        </div>
    `;

    menu.innerHTML = html;

    document.querySelector("body").prepend(menu);
}
addMenu();

window.addEventListener("load", function() {
    document.getElementById("home-button").addEventListener("click", function(){
        window.location.href = "/";
    });
});