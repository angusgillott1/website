//------------------
// Launch app button
//------------------

function launchApp()
{
    document.body.classList.add("app-launched");
}

//------------------
// Booting application
//------------------

window.onload = function()
{
    document.getElementById("launch-app-button").addEventListener("click", launchApp);
    launchApp();
    resizeCanvas();
}

//------------------
// Game of Life logic
//------------------

let cells = [];
let calculationMode = "JavaScript";

function toggleCalculationMode()
{
    if(calculationMode == "JavaScript") {
        calculationMode = "Web Assembly";
    }
    else {
        calculationMode = "JavaScript";
    }
}

function lifeTick()
{
    let neighbourCounts = new Array(cells.length).fill(0);
    
    for(let y = 0; y < graphicsCanvasHeight; y++) {
        for(let x = 0; x < graphicsCanvasWidth; x++) {
            let cn = cellIndex(x, y);
            if(!cells[cn]) { continue; }
            for(let i = -1; i <= 1; i++) {
                for(let j = -1; j <= 1; j++) {
                    if(i == 0 && j == 0) { continue; }
                    
                    let ci = cellIndex(x + i, y + j);
                    if(ci == -1) { continue; }
                    
                    neighbourCounts[ci] += 1;
                }               
            }
        }
    }

    for(let y = 0; y < graphicsCanvasHeight; y++) {
        for(let x = 0; x < graphicsCanvasWidth; x++) {
            let ci = cellIndex(x, y);
            
            let neighbours = neighbourCounts[ci];

            if(neighbours < 2 || neighbours > 3) {
                cells[ci] = 0;
            }

            if(neighbours == 3) {
                cells[ci] = 1;
            }
        }
    }
}

// Adjusts the data structure for the life cells, to maintain 1:1 pixel to cell mapping
function resizeLifeWorld(oldWidth, oldHeight, newWidth, newHeight)
{
    let newCells = new Array(newWidth * newHeight).fill(0);

    // copy across data
    for(let y = 0; y < oldHeight; y++) {
        if(y >= newHeight) continue;

        for(let x = 0; x < oldWidth; x++) {
            if(x >= newWidth) continue;

            newCells[cellIndex(x, y, newWidth)] = cells[cellIndex(x, y, oldWidth)];
        }
    }    

    cells = newCells;
}

// Returns the index in the one dimensional array of the cell at coordinate
// Returns -1 if cell is out of canvas bounds (hence not in array)
function cellIndex(x, y, canvasWidth = -1)
{
    if(x < 0 || x >= graphicsCanvasWidth || y < 0 || y > graphicsCanvasHeight) { return -1; }
    
    if(canvasWidth == -1) {
        canvasWidth = graphicsCanvasWidth;
    }

    return canvasWidth * y + x;    
}

function fillWithNoise()
{
    for(let i = 0; i < cells.length; i++) {
        cells[i] = (Math.random() < 0.12 ? 1 : 0);
    }
}

function fillBorder()
{
    for(let i = 0; i < graphicsCanvasWidth; i++) {
        cells[i] = 1;
        cells[graphicsCanvasWidth * (graphicsCanvasHeight - 1) + i] = 1;
    };

    for(let i = 0; i < graphicsCanvasHeight; i++) {
        cells[graphicsCanvasWidth * i] = 1;
        cells[graphicsCanvasWidth * i + graphicsCanvasWidth - 1] = 1;
    }

    cells[graphicsCanvasWidth * graphicsCanvasHeight - 1] = 1;
}

//------------------
// Input
//------------------

function handleLeftClick(event)
{
    fillWithNoise();
    renderWorld();
}
document.getElementById("graphics-canvas").addEventListener("click", handleLeftClick);

//------------------
// Canvas operations
//------------------

let graphicsCanvasWidth = 0;
let graphicsCanvasHeight = 0;
let timeOutActive = false;

let lastFrameTime = 0;
let lastFrameRenderAt = 0;

let mouseX = 0;
let mouseY = 0;

function storeMousePosition(event) {
    if(!event) {
        mouseX = 0;
        mouseY = 0;
        return;
    }

    let canvas = event.target;

    if(!canvas) {
        mouseX = 0;
        mouseY = 0;
        return;
    }

	let canvasRect = canvas.getBoundingClientRect();

	mouseX = (event.clientX - canvasRect.left) * (canvas.width / canvasRect.width);
    mouseY = (event.clientY - canvasRect.top) * (canvas.height / canvasRect.height);
}
window.addEventListener('mousemove', storeMousePosition);

function resizeCanvas()  // Resets canvas width and height
{
    const widthPercentageSize = 0.99;
    const heightPercentageSize = 0.99;
    
    const graphicsCanvas = document.getElementById("graphics-canvas");

    if(!graphicsCanvas) {
        return;
    }

    let newGraphicsCanvasWidth = Math.floor(window.innerWidth * widthPercentageSize);
    let newGraphicsCanvasHeight = Math.floor(window.innerHeight * heightPercentageSize);

    resizeLifeWorld(graphicsCanvasWidth, graphicsCanvasHeight, newGraphicsCanvasWidth, newGraphicsCanvasHeight);

    graphicsCanvasWidth = newGraphicsCanvasWidth;
    graphicsCanvasHeight = newGraphicsCanvasHeight;
    
    graphicsCanvas.width = graphicsCanvasWidth;
    graphicsCanvas.height = graphicsCanvasHeight;
    
    fillWithNoise();
    renderWorld();
}
window.addEventListener('resize', resizeCanvas);

function renderWorld()
{
    let currentTime = Date.now();
    lastFrameTime = currentTime - lastFrameRenderAt;
    lastFrameRenderAt = currentTime;
    let lastFrameRate = 1000.0 / lastFrameTime;

    lifeTick();
    
    let c = document.getElementById("graphics-canvas");
    
    if(!c) {
        return;
    }

    let ctx = c.getContext("2d");

    ctx.fillStyle = 'rgb(0, 0, 0)';
    ctx.fillRect(0, 0, graphicsCanvasWidth, graphicsCanvasHeight);

    // Draw life
    let canvasImageData = ctx.getImageData(0, 0, graphicsCanvasWidth, graphicsCanvasHeight);
    
    for(let i = 0; i < graphicsCanvasWidth * graphicsCanvasHeight; i++) {
        canvasImageData.data[i * 4 + 2] = (cells[i] * 255); // set blue byte to 255
    }

    ctx.putImageData(canvasImageData, 0, 0);

    // Draw cursor

    ctx.fillStyle = 'rgb(255, 255, 255)';
    ctx.fillRect(mouseX - 2, mouseY - 2, 5, 5);

    // Draw Profiling stats

    ctx.font = '16px monospace';
    ctx.fillStyle = 'rgb(255, 255, 255)';

    ctx.fillText('Performance: ' + lastFrameTime + 'ms / ' + Math.floor(lastFrameRate) + 'FPS', 12, 24);
    ctx.fillText('Calculation mode: ' + calculationMode, 12, 48);

    // Draw controls

    if(!timeOutActive) {
        setTimeout(function () {
            timeOutActive = false;
            renderWorld();
        }, 1);
        timeOutActive = true;
    }
}